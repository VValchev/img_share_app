from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from .models import User, Image, Post, Like



'''User List serializers'''


class UserList(serializers.ModelSerializer):

    follows_cnt = serializers.SerializerMethodField()
    followers_cnt = serializers.SerializerMethodField()

    class Meta:
        model = User
        # fields = ('follows', 'first_name', 'last_name', 'followers', 'follows_cnt', 'followers_cnt')
        fields = ('first_name', 'last_name', 'follows_cnt', 'followers_cnt')

    def get_follows_cnt(self, obj):
        return obj.follows.count()

    def get_followers_cnt(self, obj):
        return obj.followers.count()


'''All Image serializers'''


class AllImageSerializer(serializers.ModelSerializer):

    class Meta:
        model = Image
        fields = ('img_url',)


'''Image serializers'''


class UserImagesSerializer(serializers.ModelSerializer):

    class Meta:
        model = Image
        fields = ('img_url',)



'''Like serializers'''


class LikeSerializer(serializers.ModelSerializer):
    class Meta:

        model = Like
        fields = ('id', 'created' )



'''POST serializers'''


class PostSerializer(serializers.ModelSerializer):
    post_img = AllImageSerializer()
    likes = LikeSerializer(many=True)

    class Meta:
        model = Post
        fields = ('id', 'post_text', 'post_img', 'likes')


    def create(self, validated_data):

        post_img_url = validated_data.pop('post_img')['img_url']
        img = Image.objects.create(img_url=post_img_url)
        post = Post.objects.create(post_img=img, **validated_data)
        return post





'''User serializers'''


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = Image
        fields = ('id', )




'''Follows serializers'''



class PostImageSerializer(serializers.ModelSerializer):
    post_img = AllImageSerializer()


    class Meta:
        model = Post
        fields = ('id', 'post_img',)


