from django.db import models


class Image(models.Model):

    img_url = models.CharField(max_length=100)

    created     = models.DateTimeField(auto_now_add=True)
    updated     = models.DateTimeField(auto_now=True)


class User(models.Model):
    first_name  = models.CharField(max_length=100)
    last_name   = models.CharField(max_length=100)
    follows     = models.ManyToManyField("self", symmetrical=False, related_name="followers")
    post        = models.ManyToManyField('Post', )
    likes       = models.ManyToManyField("Like", related_name="user")

    created     = models.DateTimeField(auto_now_add=True)
    updated     = models.DateTimeField(auto_now=True)


class Post(models.Model):
    post_text   = models.TextField()
    post_img    = models.ForeignKey('Image', on_delete=models.CASCADE, related_name="img")

    created     = models.DateTimeField(auto_now_add=True)
    updated     = models.DateTimeField(auto_now=True)


class Like(models.Model):
    post        = models.ForeignKey('Post', on_delete=models.CASCADE, related_name="likes", null=True, blank=True)

    created     = models.DateTimeField(auto_now_add=True)
    updated     = models.DateTimeField(auto_now=True)



