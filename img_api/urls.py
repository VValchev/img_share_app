from django.urls import path
from rest_auth.views import LogoutView
from .api import ImageList, UserList, PostList, UserImagesList, post_follows, post_unfollows, post_like, post_unlike

urlpatterns = [

    # IMAGES URLS
    path('api/images', ImageList.as_view()),
    path('api/images/users/<user_id>', UserImagesList.as_view()), #ok

    # POST URLS
    path('api/posts', PostList.as_view()), #ok

    # USER URLS
    path('api/users', UserList.as_view()), #ok
    path('api/users/follows/<int:id>', post_follows),  # ok
    path('api/users/unfollows/<int:id>', post_unfollows),  # ok

    # USER Likes
    path('api/users/likes/<int:id>', post_like), # post
    path('api/users/unlikes/<int:id>', post_unlike),  # post



]