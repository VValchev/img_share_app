import xlwt as xlwt
from rest_auth.views import LoginView, APIView
from rest_auth.views import LogoutView
from rest_framework import generics, permissions
from rest_framework.decorators import api_view
from rest_framework.exceptions import ValidationError
from rest_framework.response import Response
from .models import User, Image, Post, Like
from .serializers import PostSerializer, AllImageSerializer, UserImagesSerializer, UserList, \
    PostImageSerializer
from django.db.models import Count

'''List all Images'''


class ImageList(generics.ListCreateAPIView):

    serializer_class = AllImageSerializer
    queryset = Image.objects.all()


'''List of images for the current user follow'''


class UserImagesList(generics.ListCreateAPIView):

    serializer_class = PostImageSerializer



    def get_queryset(self):

        print('aaaa', self.kwargs['user_id'])
        user_id = self.kwargs['user_id']

        aaa = 3 #

        cur_user = User.objects.filter(id=user_id)
        user_posts = cur_user[0].post.all().order_by('-created')

        followers = cur_user[0].followers.all()

        if aaa in list(followers.values_list('id', flat=True)):
            print("yes")
            return user_posts
        else:
            return User.objects.none()






'''POST views'''


class PostList(generics.ListCreateAPIView):

    serializer_class = PostSerializer
    queryset = Post.objects.all().annotate(l_count=Count('likes')).order_by('-l_count')




'''Users follow views'''


class UserList(generics.ListCreateAPIView):

    serializer_class = UserList
    queryset = User.objects.all()




@api_view(["POST"])
def post_follows(request, id):

    loged_user = User.objects.get(id=1)
    user = User.objects.get(id=id)
    loged_user.follows.add(user)
    return Response({'success': True})


@api_view(["POST"])
def post_unfollows(request, id):

    loged_user = User.objects.get(id=1)
    user = User.objects.get(id=id)
    loged_user.follows.remove(user)
    return Response({'success': True})

@api_view(["POST"])
def post_like(request, id):

    loged_user = User.objects.get(id=1)
    like = Like.objects.get(id=id)
    like.user.add(loged_user)
    return Response({'success': True})


@api_view(["POST"])
def post_unlike(request, id):

    loged_user = User.objects.get(id=1)
    like = Like.objects.get(id=id)
    like.user.remove(loged_user)
    return Response({'success': True})